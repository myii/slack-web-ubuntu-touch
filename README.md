# CutTheRope for Ubuntu Touch

This project is partially a fork of the original Ubuntu Touch CutTheRope game [Zeptolab](https://www.zeptolab.com/) once wrapped for Ubuntu Touch.

However, this version is using a newer game than the original Ubuntu Touch release, basing it on a source found on [GitHub](https://github.com/windyboy1704/CutTheRope) which contains a copy backed up before Zeptolab deleted it.

Moreover this game is not dependant on Oxide, though it took a few tweaks to work under QML WebEngine.
The game should also scale up nicely on various screen resolutions, though only a few options were actually tested.

## Prerequisites

This repository doesn't contain the game itself. That is hosted in a different repository and in order for this game to run you need to add it to the source. The following should be enough:

* `cd` to the project root directory
* `git submodule init`
* `git submodule update`

### Custom game source

For example in case the linked game repository is taken down you can supply an alternate game source (extracted from a purchased game or if that's not an option then found anywhere on the internet). In order to proceed place the web-based game itself including all additional sub-directoried to:

```
projectRootDirectory/game/CutTheRope/
```

Then ensure the main HTML file to launch is called `index.html` and is located directly under the `CutTheRope` directory. If not, rename it. Unless there are major differences in the game it should be able to run just fine.

## Building

To build this for Ubuntu Touch, you need to have Clickable installed. Instruction on how to install it can be found on [Clickable homepage](http://clickable.bhdouglass.com/en/latest/).

Then just run the following command in the project root directory:

```
clickable
```

In order to launch this directly on a PC, try:

```
clickable desktop
```

## Debugging

To run and debug this directy on a phone/tablet via USB connection execute:

```
clickable launch && clickable logs
```

Or if you have SSH enabled on your device, you may want to run:

```
clickable --ssh IP_ADDRESSorHOSTNAME && clickable logs --ssh IP_ADDRESSorHOSTNAME
```

## Issues on mobile

Unfortunately there is no hardware acceleration in QtWebengine on UT so the game tends to lag a bit, but it is playable at least on Meizu MX4. On lower-specs devices like the BQ Aquaris 4.5 or Nexus 4 the game may be hardly playable without HW acceleration.

## License

The wrapper itself is released under GPL. There is nothing specific on that and is mostly a custom work with some inspiration taken over from the original UT game.

However, assets present in the `assets` directory are purely Zeptolab's ownership and although they could have been replaced with something else the game itself is theirs and I don't want to detach it from them. All the kudos for this game belongs to Zeptolab.
